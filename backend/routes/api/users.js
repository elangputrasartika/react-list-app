const express = require('express');

const router = express.Router();
const { UserController } = require('../index');
// const { catchErrors } = require('../../middleware/error-handler');

router.get('/users', UserController.getUsers);
// router.post('/create', UserController.createUser);

module.exports = router;
