// const mongoose = require("mongoose");
const UserRepo = require("../repo/UserRepo");


exports.getUsers = async (req, res) => {
    try {
        const { page = 1, limit = 10 } = req.query;
        const result = await UserRepo.getAll(page, limit);

        return res.status(200).json({
            status: "success",
            metadata: result[0].metadata,
            data: result[0].data
        });
    } catch (e) {
        return res.status(400).json({ status: "error", message: e });
    }
};
