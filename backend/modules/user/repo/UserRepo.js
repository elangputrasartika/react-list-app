// const mongoose = require('mongoose');
const User = require('../model/User');
// const merge = (a, b, i=0) => a.splice(i, 0, ...b) && a;

const paginationAttr  = (page, limit = 10) => {
    return {
        $facet: {
            metadata: [
                { $count: "total" },
                { $addFields: { page: Number(page) } },
                { $addFields: { limit: Number(limit) } }
            ],
            data: [ { $skip: ((Number(page) * Number(limit)) - Number(limit)) }, { $limit: Number(limit) } ]
        }
    }
};

exports.getAll = async (page = 1, limit = 10) => {
    const response = await User.aggregate([
        { $sort: { 'nama': 1 } },
        paginationAttr(page, limit)
    ]);

    return response;
}

function array_map(raw, allowed, add = '') {
    const result = {}
    allowed.map(key => {
        if (Object.keys(raw).includes(key)) {
            result[key] = add + raw[key]
        }
    });

    return result;
}
